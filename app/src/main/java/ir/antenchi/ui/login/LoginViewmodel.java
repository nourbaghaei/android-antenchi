package ir.antenchi.ui.login;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import androidx.lifecycle.ViewModel;

import ir.antenchi.api.services.Service;
import ir.antenchi.base.AntenchiApplication;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class LoginViewmodel extends ViewModel {
    SendDataToActivity sendData;
    private String phone;
    private Service service;
    private final CompositeDisposable disposable;

    public LoginViewmodel() {
        disposable=new CompositeDisposable();
    }

    public void getVerifyCode()
    {
        Log.d("TAG", "getVerifyCode: ");
        service= AntenchiApplication.getClient().create(Service.class);
       disposable.add( service.getVerifyCode(phone).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(o -> {
                   Log.d("TAG", "getVerifyCode: "+o);
//                  Meta meta = o.getMeta();
//                 if (meta!=null)
//                 {
//                     if(meta.getCode()!=null)
//                     {
//                         int code=meta.getCode();
//                         if(code==200)
//
//                     }

//                 }
               sendData.startVerifyActivity();
               },
               throwable -> {}));
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            Log.d("tag2121", "onTextChanged: " + charSequence);
        }

        @Override
        public void afterTextChanged(Editable editable) {
            sendData.sendPhoneNumber(editable.toString());
            phone=editable.toString();

        }
    };


    public TextWatcher getTextWatcher() {
        return textWatcher;
    }

    public void setTextWatcher(TextWatcher textWatcher) {
        this.textWatcher = textWatcher;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }

    public void setSendDataInterface(SendDataToActivity sendData)
    {
        this.sendData=sendData;
    }

    interface SendDataToActivity{
        void sendPhoneNumber(String phone);
        void startVerifyActivity();
    }
}
