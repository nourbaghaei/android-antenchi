package ir.antenchi.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;


import java.util.regex.Pattern;

import ir.antenchi.R;
import ir.antenchi.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity implements LoginViewmodel.SendDataToActivity {
ActivityLoginBinding binding;
LoginViewmodel loginViewmodel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding= DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginViewmodel= new ViewModelProvider(this).get(LoginViewmodel.class);
        binding.setViewmodel(loginViewmodel);
        loginViewmodel.setSendDataInterface(this);

    }

    @Override
    public void sendPhoneNumber(String phone) {
        if (Pattern.matches("(0)(9)[0-9]{9}",phone))
        {

            Log.d("TAG22", "sendPhoneNumber: "+phone);
            binding.btnLogin.setEnabled(true);
        }else{
            binding.btnLogin.setEnabled(false);
        }
    }

    @Override
    public void startVerifyActivity()
    {
        Log.d("TAG11", "startVerifyActivity: ");
        VerifyCodeActivity.start(this);
    }


}