package ir.antenchi.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import ir.antenchi.R;
import ir.antenchi.databinding.ActivityVerifyCodeBinding;


public class VerifyCodeActivity extends AppCompatActivity {
ActivityVerifyCodeBinding binding;

  public static void start(Context context) {
      Log.d("TAG11", "start: ");
      Intent starter = new Intent(context, VerifyCodeActivity.class);
      context.startActivity(starter);
  }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding= DataBindingUtil.setContentView(this, R.layout.activity_verify_code);
        binding.setActivity(this);
        timer().start();
    }

    private CountDownTimer timer()
    {
        return new CountDownTimer(60000,1000) {
            @Override
            public void onTick(long l) {
            binding.setTimerVisibility(true);
            binding.setTimer(String.valueOf(l/1000));

            }

            @Override
            public void onFinish() {

                binding.setTimerVisibility(false);
            }
        };
    }

    //back button click listener
    public void backPressed(){
        timer().cancel();
        finish();
    }

}