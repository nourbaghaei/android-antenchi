package ir.antenchi.base;

import android.app.Application;
import android.util.Log;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import ir.antenchi.helper.Constants;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class AntenchiApplication extends Application {
    private static Retrofit retrofit;
    @Override
    public void onCreate() {
        super.onCreate();
    }

    private static OkHttpClient buildClient()
    {
        Log.d("TAG", "buildClient: ");
        HttpLoggingInterceptor interceptor=new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder okHttpBuilder= new OkHttpClient.Builder();
        okHttpBuilder.addInterceptor(new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request.Builder requestBuilder=chain.request().newBuilder();
                requestBuilder.addHeader("Content-Type","application/json");
                return chain.proceed(requestBuilder.build());
            }
        }).addInterceptor(interceptor);
        okHttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        return okHttpBuilder.build();
    }

    public static Retrofit getClient()
    {
        Log.d("TAG", "getClient: ");
        if (retrofit==null)
        {
            retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(Constants.SERVER_URL)
                    .client(buildClient())
                    .build();
        }
        return retrofit;
    }
}
