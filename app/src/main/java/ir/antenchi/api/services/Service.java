package ir.antenchi.api.services;

import io.reactivex.Single;
import ir.antenchi.api.response.ResLogin;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Service {


    @FormUrlEncoded
    @POST("auth/sign")
    Single<ResLogin> getVerifyCode(@Field("mobile") String phone);

    @FormUrlEncoded
    @POST("auth/verify")
    Single<ResLogin> auth(@Field("mobile") String mobile,@Field("code") int code);

}
