package ir.antenchi.api.response

import com.google.gson.annotations.SerializedName

data class ResLogin(

		@field:SerializedName("data")
	val data: Data? = null,

		@field:SerializedName("meta")
	val meta: Meta? = null
)

data class Data(

	@field:SerializedName("name")
	val name: Any? = null
)

data class Meta(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("method")
	val method: Any? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("url")
	val url: Any? = null
)
