package ir.antenchi.helper;

import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import androidx.databinding.BindingAdapter;

public class BindingAdapters {


   @BindingAdapter("textChangedListener")
    public static void bindTextWatcher(EditText editText, TextWatcher textWatcher) {
        editText.addTextChangedListener(textWatcher);
    }

    @BindingAdapter({"onClickListener"})
    public static void setClickListener(View view, View.OnClickListener clickListener) {
        view.setOnClickListener(clickListener);

    }
}
